## Changelog

##### version 1.5.4 (20/06/2022)
- Better support on shapefile and geopackage

##### version 1.5.3 (10/11/2021)
- add command line interface
- add geopackage format

##### version 1.5.2 (12/11/2020)
- update libs for Java 9 and greater compatibility
- migrate CI to sourcesup

##### version 1.5
