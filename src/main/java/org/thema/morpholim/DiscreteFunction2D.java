/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.morpholim;

import Jama.Matrix;
import org.jfree.data.function.Function2D;

/**
 * This class represents a discrete function defined by a set of 2D coordinates.
 * 
 * @author Gilles Vuidel
 */
public class DiscreteFunction2D implements Function2D {
    private final double[] x;
    private final double[] y;

    /**
     * Creates a new discrete function.
     * The array parameters are not dupplicated
     * @param x the absciss values
     * @param y the ordinate values
     */
    public DiscreteFunction2D(double[] x, double[] y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double getValue(double xval) {
        for (int i = 0; i < x.length; i++) {
            if (x[i] == xval) {
                return y[i];
            }
        }
        return Double.NaN;
    }

    /**
     * @return absciss values
     */
    public double[] getX() {
        return x;
    }

    /**
     * @return ordinate values
     */
    public double[] getY() {
        return y;
    }

    /**
     * @return the number of points of the function
     */
    public int getNbPoint() {
        return x.length;
    }

    /**
     * Calculate the polynomial regression of degree deg for this function.
     * The regression is an ordinary least square
     * @param deg the degree of the polynom
     * @return a polynomial function adjusting this function
     */
    public PolynomFunction2D OLSRegression(int deg) {
        Matrix mat = new Matrix(getNbPoint(), deg + 1);
        for (int j = 0; j < deg + 1; j++) {
            for (int i = 0; i < getNbPoint(); i++) {
                mat.set(i, j, Math.pow(x[i], j));
            }
        }
        Matrix bmat = new Matrix(getNbPoint(), 1);
        for (int i = 0; i < getNbPoint(); i++) {
            bmat.set(i, 0, y[i]);
        }
        double[] coef = mat.solve(bmat).getRowPackedCopy();
        return new PolynomFunction2D(coef);
    }

    /**
     * Calculate the sum of squared error between this function and the polynom f
     * @param f the polynomial function
     * @return the sum of squarred error
     */
    public double getSSE(PolynomFunction2D f) {
        double sse = 0;
        for (int i = 0; i < getNbPoint(); i++) {
            sse += Math.pow(y[i] - f.getValue(x[i]), 2);
        }
        return sse;
    }

    /**
     * Calculate the R2 between this function and the polynom f
     * @param f the polynomial function
     * @return the R2
     */
    public double getR2(PolynomFunction2D f) {
        double mean = 0;
        for (int i = 0; i < getNbPoint(); i++) {
            mean += y[i];
        }
        mean /= getNbPoint();
        double scr = 0;
        double sct = 0;
        for (int i = 0; i < getNbPoint(); i++) {
            scr += Math.pow(y[i] - f.getValue(x[i]), 2);
            sct += Math.pow(y[i] - mean, 2);
        }
        return 1 - scr / sct;
    }
    
}
