/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.morpholim;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;


/**
 * Calculate the threshold representing the main curvature.
 * Adjust the log-log curve with a polynom, calculate the curvature of the polynom, find the curvature extremum between a given range.
 * @author Gilles Vuidel
 */
public class Threshold {
    public static final int PREC = 1000;

    /**
     * Calculate a polynomial regression of a given degree and calculate the curvature of polynom.
     * 
     * Effectue une régression polynomiale d'un degre donné 
     * et détermine la fonction courbure du polynome obtenu
     */
    public class Estimation {
        private int degre;
        private PolynomFunction2D f;
        private double r2, bic;

        private DiscreteFunction2D kappa;
        private TreeMap<Double, Double> maxCurv;

        /**
         * Creates an new estimation of the curvature for a given polynom degree
         * @param degre the degree of the polynom
         */
        public Estimation(int degre) {
            this.degre = degre;

            int n = fClust.getNbPoint();
            f = fClust.OLSRegression(degre);
            r2 = fClust.getR2(f);
            double sse = fClust.getSSE(f);
            bic = (degre+1)*Math.log(n) + n*Math.log(sse/n);

            DiscreteFunction2D df = f.derive().discrete(xreg);
            DiscreteFunction2D d2f = f.derive().derive().discrete(xreg);
            double [] ka = new double[xreg.length];
            for(int i = 0; i < ka.length; i++) {
                ka[i] = d2f.getY()[i] / Math.pow(Math.pow(df.getY()[i], 2) + 1, 1.5);
            }
            kappa = new DiscreteFunction2D(xreg, ka);
            
            maxCurv = new TreeMap<>();
            for(int i = 1; i < ka.length-1; i++) {
                double d1 = (ka[i+1]-ka[i]);
                double d2 = (ka[i]-ka[i-1]);
                if(d1 * d2 <= 0 && (d1 != 0 || d2 != 0)) {
                    maxCurv.put(xreg[i], ka[i]);
                }
            }
        }

        /**
         * Return the absciss of the absolute maximum curvature between xmin and xmax
         * @param xmin the min absciss
         * @param xmax the max absciss
         * @return the absciss with the highest absolute curvature
         */
        public double getThreshold(double xmin, double xmax) {
            double maxCmax = 0;
            double threshold = 0;
            for(Double x : maxCurv.subMap(xmin, true, xmax, true).keySet()) {
                if(Math.abs(maxCurv.get(x)) > maxCmax) {
                    maxCmax = Math.abs(maxCurv.get(x));
                    threshold = x;
                }
            }

            return threshold;
        }
    }

    private DiscreteFunction2D fClust;
    private double [] xreg;
    private double xmin, xmax;
    private List<Estimation> estimations;

    /**
     * Creates a new instance of Threshold for the curve (xc, yc)
     * 
     * @param xc the abscisses 
     * @param yc the ordinates 
     */
    public Threshold(SortedMap<Double, Double> curve) {
        double [] x = new double[curve.size()];
        double [] y = new double[curve.size()];
        int nb = 0;
        for(double xi : curve.keySet()) {
            if(xi > 0) {
                x[nb] = Math.log(xi);
                y[nb] = Math.log(curve.get(xi));
                nb++;
            }
        }
        
        fClust = new DiscreteFunction2D(Arrays.copyOf(x, nb), Arrays.copyOf(y, nb));

        xreg = new double[PREC+1];
        double d = (fClust.getX()[fClust.getNbPoint()-1] - fClust.getX()[0]) / PREC;
        for(int i = 0; i <= PREC; i++) {
            xreg[i] = i * d + fClust.getX()[0];
        }
        xmin = xreg[0];
        xmax = xreg[xreg.length-1];
        xmin = xmin + (xmax-xmin) * 0.2;
        xmax = xmax - (xmax-xmin) * 0.1;
    }

    /**
     * Calculate all estimations between degree one and degMax
     * @param degMax the maximum polynomial degree
     */
    public void calcThreshold(int degMax) {
        estimations = new ArrayList<>(degMax);
        for(int deg = 1; deg <= degMax; deg++) {
            estimations.add(new Estimation(deg));
        }
    }

    /**
     * @return the minimal absciss for the threshold
     */
    public double getMinMargin() {
        return Math.exp(xmin);
    }
    /**
     * @return the maximal absciss for the threshold
     */
    public double getMaxMargin() {
        return Math.exp(xmax);
    }

    /**
     * Set the range for the threshold
     * @param min the minimal absciss
     * @param max the maximal absciss
     */
    public void setMargin(double min, double max) {
        xmin = Math.log(min);
        xmax = Math.log(max);
    }

    /**
     * Return the absolute maximal curvature for the estimation of degree deg
     * @param deg the polynomial degree
     * @return the absciss threshold
     */
    public double getThreshold(int deg) {
        return Math.exp(estimations.get(deg-1).getThreshold(xmin, xmax));
    }
    
    /**
     * The BIC of all polynomial estimations ordered by degree
     * @return the Bayesian Information Criterions for each regressions
     */
    public List<Double> getBICs() {
        List<Double> bic = new ArrayList<>();
        for(Estimation estim : estimations) {
            bic.add(estim.bic);
        }
        return bic;
    }

    /**
     * The R2 of all polynomial estimations ordered by degree
     * @return the R2 for each regressions
     */
    public List<Double> getR2s() {
        List<Double> r2 = new ArrayList<>();

        for(Estimation estim : estimations) {
            r2.add(estim.r2);
        }
        return r2;
    }

    /**
     * @return the input curve in log-log
     */
    public DiscreteFunction2D getClusterCurve() {
        return fClust;
    }

    /**
     * The estimation curve (polynomial curve) of degree deg
     * @param deg the polynomial degree
     * @return the estimation curve
     */
    public DiscreteFunction2D getEstimCurve(int deg) {
        return estimations.get(deg-1).f.discrete(xreg);
    }

    /**
     * The curvature for the polynom of degree deg
     * @param deg the polynomial degree
     * @return the curvature
     */
    public DiscreteFunction2D getCurvature(int deg) {
        return estimations.get(deg-1).kappa;
    }

    /**
     * The points where curvature derivative changes its sign for the polynom of degree deg
     * @param deg the polynomial degree
     * @return the extrema curvature points
     */
    public DiscreteFunction2D getMaxCurvature(int deg) {
        TreeMap<Double, Double> maxCurv = estimations.get(deg - 1).maxCurv;
        double [] x = new double[maxCurv.size()];
        double [] y = new double[maxCurv.size()];
        int i = 0;
        for(Double d : maxCurv.keySet()) {
            x[i] = d;
            y[i] = estimations.get(deg - 1).f.getValue(d);
            i++;
        }
        return new DiscreteFunction2D(x, y);
    }
}
