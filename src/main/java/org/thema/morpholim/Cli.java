/*
 * Copyright (C) 2021 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.morpholim;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.operation.buffer.BufferParameters;
import org.thema.common.CliTools;
import org.thema.common.Config;
import org.thema.common.parallel.BufferForkJoinTask;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.swing.TaskMonitor;
import org.thema.data.IOFeature;
import org.thema.data.feature.DefaultFeature;

/**
 *
 * @author gvuidel
 */
public class Cli {
    
    
    public static void execute(String[] argArr) throws IOException {
        if(argArr[0].equals("--help")) {    
            System.out.println("Usage :\njava -jar morpholim.jar [-proc n]\n"  +
                    "--cluster layer=file radius=val coef=val [prec=val] res=filename.txt\n");
            return;
        }
        
        
        TaskMonitor.setHeadlessStream(new PrintStream(File.createTempFile("java", "monitor")));

        List<String> args = new ArrayList<>(Arrays.asList(argArr));
        
        // parallel options
        while(!args.isEmpty() && !args.get(0).startsWith("--")) {
            String p = args.remove(0);
            switch (p) {
                case "-proc":
                    int n = Integer.parseInt(args.remove(0));
                    Config.setParallelProc(n);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown option " + p);
            }
        }
        if(args.isEmpty()) {
            throw new IllegalArgumentException("Need command");
        }
        
        while(!args.isEmpty()) {
            String arg = args.remove(0);
            switch(arg) {
                case "--cluster":
                    cluster(args);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown command : " + arg);
            }
        }
    }

    private static void cluster(List<String> args) throws IOException {
        Map<String, String> params = CliTools.extractAndCheckParams(args, Arrays.asList("layer", "radius", "coef", "res"), Arrays.asList("prec"));
        File layer = new File(params.get("layer"));
        int prec = 8;
        if(params.containsKey("prec")) {
            prec = Integer.parseInt(params.get("prec"));
        }
        SortedMap<Double, Double> clusters = calcClusters(layer, Double.parseDouble(params.get("radius")), Double.parseDouble(params.get("coef")), prec, System.out);
        
        saveCurve(layer, new File(params.get("res")), clusters);
        
    }
    
    public static SortedMap<Double, Double> calcClusters(File f, double radius, double coef, int precision, Appendable log) throws IOException {
        ArrayList<Geometry> geoms = new ArrayList<>();
        TreeMap<Double, Double> clusters = new TreeMap<>();
        List<DefaultFeature> features = IOFeature.loadFeatures(f);

        for(DefaultFeature fe : features) {
            geoms.add(fe.getGeometry());
        }
        boolean valid = true;
        for(int i = 0; i < geoms.size(); i++) {
            if(!geoms.get(i).isValid()) {
                geoms.set(i, geoms.get(i).buffer(0));
                valid = false;
            }
        }
        if(!valid) {
            log.append("Some geometries are not valid\n");
        }
        Geometry geom = new GeometryFactory().buildGeometry(geoms);
        geom = BufferForkJoinTask.threadedBuffer(geom, 0);

//                    saveGeom(geom, new File(f.getParent(), f.getName() + "_union.shp"), null);

        log.append(geom.getEnvelopeInternal()+"\n");
        Geometry initGeom = geom;
        log.append(initGeom.getNumPoints() + " points\n");
        log.append("0 - " + geom.getNumGeometries() + "\n");
        long start = System.currentTimeMillis();
        int n = geom.getNumGeometries();
        clusters.put(0.0, (double)n);
        double sub = 0;
        while(n > 1) {
            geom = BufferForkJoinTask.threadedBuffer(initGeom, radius-sub,
                    new BufferParameters(precision));
            n = geom.getNumGeometries();
            log.append(2*radius + " - " + n + "\n");
            clusters.put(2*radius, (double)n);
            if(Config.getParallelProc() == 1 && 
                    initGeom.getNumGeometries() / n > 2*2*precision) {
                initGeom = geom;
                sub = radius;
                log.append("We keep the buffer.. -> " + initGeom.getNumPoints() + " points\n");
            }
            geom = null; // pour que le GC puisse le libérer le plus tôt possible
            radius *= coef;
            // reduce the radius precision to four digits
            radius = new BigDecimal(radius).round(new MathContext(4, RoundingMode.HALF_UP)).doubleValue();
        }
        log.append((System.currentTimeMillis() - start)/1000 + " sec\n");
        for(Double eps : clusters.keySet()) {
            log.append(eps + "\t" + clusters.get(eps) + "\n");
        }
        
        return clusters;
    }
    
    public static void saveCurve(File layer, File res, SortedMap<Double, Double> clusters) throws IOException {
        try (FileWriter w = new FileWriter(res)) {
            w.write(layer.getAbsolutePath());
            w.write("\nDist\tNbClust\n");
            for(Double eps : clusters.keySet()) {
                w.write(eps + "\t" + clusters.get(eps) + "\n");
            }
        } 
    }
}
