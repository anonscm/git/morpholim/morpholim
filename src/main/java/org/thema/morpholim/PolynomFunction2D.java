/*
 * Copyright (C) 2014 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.morpholim;

import org.jfree.data.function.Function2D;

/**
 * This class represents a polynomial function.
 * 
 * @author Gilles Vuidel
 */
public class PolynomFunction2D implements Function2D {
    private final double[] coef;

    /**
     * Creates a new polynomial function.
     * coef[0] corresponds to the constant
     * coef[1] corresponds to the coefficient of degree 1
     * etc...
     * The degree of the polynom is equal to coef.length-1
     * @param coef 
     */
    public PolynomFunction2D(double[] coef) {
        this.coef = coef;
    }

    @Override
    public double getValue(double x) {
        double val = 0;
        for (int i = 0; i < coef.length; i++) {
            val += coef[i] * Math.pow(x, i);
        }
        return val;
    }

    /**
     * Returns a new polynomial function which is the first derivative of this.
     * @return the first derivative of this polynom
     */
    public PolynomFunction2D derive() {
        double[] c = new double[coef.length - 1];
        for (int i = 1; i < coef.length; i++) {
            c[i - 1] = coef[i] * i;
        }
        return new PolynomFunction2D(c);
    }

    /**
     * Creates a discrete function representing this polynom
     * @param x the absciss values of the returned function
     * @return a discrete function
     */
    public DiscreteFunction2D discrete(double[] x) {
        double[] y = new double[x.length];
        for (int i = 0; i < x.length; i++) {
            y[i] = getValue(x[i]);
        }
        return new DiscreteFunction2D(x, y);
    }
    
}
